<?php
    header("Content-Type: application/json; charset=utf-8");
    error_reporting(E_ALL);

    $res = array();

    $fileData = read();
    $data = unserialize($fileData);

    $userId = $_POST['user_id'];

    if (isset($data[$userId])) {
        //Забираем свои сообщения
        $res = $data[$userId];
        unset($data[$userId]);
    }

    if (isset($_POST['mess']) && $_POST['mess']) {
        //Записываем сообщения для другого клиента
        $anotherUserId = anotherUser($userId);
        $data[$anotherUserId][] = $_POST['mess'];
    }

    $fileData = serialize($data);
    write ($fileData);

    echo json_encode($res);

    function anotherUser ($userId) {
        return $userId == 1 ? 2 : 1;
    }

    function write ($params) {
        $filename = 'params.php';
        $fp = fopen($filename, "w");
        fwrite ($fp,  $params);
        fclose($fp);
    }

    function read () {
        $filename = 'params.php';
        return file_get_contents($filename);
    }

    function pre ($mess) {
        echo '<pre>';
        print_r($mess);
        echo '</pre>';
    }
?>
