<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="WebRTC code samples">
        <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1, maximum-scale=1">

        <title>Peer connection</title>

        <style>
            #remoteVideo {
                position: absolute;
                width: 100%;
                height: 100vh;
                top: 0;
                left: 0;
            }

            #localVideo {
                position: absolute;
                width: 300px;
                height: 150px;
                border: 1px solid rgba(0, 0, 0, 0.5);
                top: 30px;
                right: 30px;
            }

            .btn_block {
                position: absolute;
                bottom: 30px;
                left: 50%;
                transform: translateX(-50%);
            }
        </style>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>

    <body>
        <video id="localVideo" autoplay playsinline></video>
        <video id="remoteVideo" autoplay playsinline></video>

        <div class="btn_block">
            <button id="startButton">Start</button>
            <button id="callButton">Call</button>
            <button id="answerButton">Answer</button>
            <button id="hangupButton">Hang Up</button>
        </div>

        <script>
            let localStream;
            let remoteStream;

            let pc;
            let pc1;
            let pc2;

            let initiator = false;
            let started = false;
            let userId = 0;

            let pc_config = {"iceServers": [{"url": "stun:stun.l.google.com:19302"}]};
            let pc_constraints = {"optional": [{"DtlsSrtpKeyAgreement": true}]};

            let is_new = 1;
            let room = '123456';
            let timeout = false;

            const localVideo = document.getElementById('localVideo');
            const remoteVideo = document.getElementById('remoteVideo');
            const constraints = {
                audio: true,
                video: true
            };
            const offerOptions = {
                offerToReceiveAudio: 1,
                offerToReceiveVideo: 1
            };

            const startButton = document.getElementById('startButton');
            const callButton = document.getElementById('callButton');
            const answerButton = document.getElementById('answerButton');
            const hangupButton = document.getElementById('hangupButton');

            callButton.disabled = true;
            answerButton.disabled = true;
            hangupButton.disabled = true;

            startButton.addEventListener('click', start);
            callButton.addEventListener('click', call);
            answerButton.addEventListener('click', answer);
            hangupButton.addEventListener('click', hangup);

            function handleSuccess(stream) {
                window.stream = stream; // make stream available to browser console
                localStream = localVideo.srcObject = stream;

                callButton.disabled = false;
                answerButton.disabled = false;
            }

            function handleError(error) {
                console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
            }

			//Запрос разрешения на доступ к микрофону и камере, вывод локального на экран и динамики
            function start() {
                navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);
            }

			//Функция вызов. Устанавливает id пользователя 1. Создает offer, навешивает прослушивание событий на получение iceCandidate и обработку удаленного потока.
            async function call() {
                userId = 1;

                callButton.disabled = true;
                answerButton.disabled = true;
                hangupButton.disabled = false;

                const videoTracks = localStream.getVideoTracks();
                const audioTracks = localStream.getAudioTracks();

                pc = new RTCPeerConnection({});//{sdpSemantics: 'unified-plan'} || {sdpSemantics: 'plan-b'} || {}
                pc.addEventListener('icecandidate', e => onIceCandidateAjax(pc, e));
                pc.addEventListener('track', gotRemoteStream);

                localStream.getTracks().forEach(track => pc.addTrack(track, localStream));

                //Создаем offer и устанавливаем его для вызывающей машины
                try {
                    const offer = await pc.createOffer(offerOptions);
                    sendMessage(offer);
                    await pc.setLocalDescription(offer);
                } catch (e) {
                    console.log(`Failed to create session description: ${e.toString()}`);
                }
            }

			//Функция ответа. Устанавливает id пользователя 2. Опрашивает сервер и ищет offer от другой машины
            async function answer() {
                userId = 2;

                callButton.disabled = true;
                answerButton.disabled = true;
                hangupButton.disabled = false;

                const videoTracks = localStream.getVideoTracks();
                const audioTracks = localStream.getAudioTracks();

                pc = new RTCPeerConnection({});//{sdpSemantics: 'unified-plan'} || {sdpSemantics: 'plan-b'} || {}
                pc.addEventListener('icecandidate', e => onIceCandidateAjax(pc, e));
                pc.addEventListener('track', gotRemoteStream);

                localStream.getTracks().forEach(track => pc.addTrack(track, localStream));

                // Ничего не создаем, а начинаем долбить сервер на предмет поиска сообщений от вызывающей стороны. То же самое сделать для call и кнопка будет 1.
                sendMessage();
            }

			//Формирование sdp пакета для установки связи с удаленным браузером.
            function onIceCandidateAjax(pc, event) {
                if (event.candidate) {
                    sendMessage({type: 'candidate',                        
                        label: event.candidate.sdpMLineIndex,                     
                        id: event.candidate.sdpMid,                        
                        candidate: event.candidate.candidate});
                }
            }

			//Отправка сообщений на сервер. 
            function sendMessage(message) {
                var msgString = JSON.stringify(message);
                console.log('C->S: ' + msgString);
                $.ajax({
                    type: "post",	
                    url: "/server.php",	
                    dataType: "json",
                    crossDomain: true,
                    data: {
                        room:room,
                        user_id:userId,
                        mess:msgString,
                        is_new:is_new
                    },
                    success: function(data){
                        for (var res in data){
                            var msg = data[res];
                            processSignalingMessage(msg);
                        }
                    }
                });

                // Простейшая замена web сокетам. Годится только для теста.
                is_new = 0;
                function repeat() {
                    timeout = setTimeout(repeat, 5000);
                    sendMessage();
                }
                if (!timeout) repeat();
            }

			//Обработка ответа от удаленного браузера
            async function processSignalingMessage(message) {
                var msg = JSON.parse(message);

				//Если пришел offer создаем answer
                if (msg.type === 'offer') {
                    console.log('C<-S: get offer');
                    pc.setRemoteDescription(msg);
                    const answer = await pc.createAnswer();
                    await await pc.setLocalDescription(answer);
                    sendMessage(answer);
				//Если пришел answer устанавливаем параметры удаленной машины
                } else if (msg.type === 'answer') {
                    console.log('C<-S: get answer');
                    pc.setRemoteDescription(msg);
				//Устанавливаем значение iceCandidate
                } else if (msg.type === 'candidate') {
                    console.log('C<-S: get candidate');
                    var candidate = new RTCIceCandidate({sdpMLineIndex:msg.label, candidate:msg.candidate});
                    pc.addIceCandidate(candidate);
				//Разрываем соединение
                } else if (msg.type === 'bye') {
                    console.log('C<-S: Ending call');

                    pc.close();
                    pc = null;

                    sendMessage('{"type":"bye"}');

                    hangupButton.disabled = true;
                    callButton.disabled = false;
                    answerButton.disabled = false;
                }
            }

			//Вывод потока с удаленного браузера.
            function gotRemoteStream(e) {
                if (remoteVideo.srcObject !== e.streams[0]) {
                    remoteVideo.srcObject = e.streams[0];
                }
            }

            function hangup() {
                sendMessage("{\"type\":\"bye\"}");
            }
        </script>
    </body>
</html>